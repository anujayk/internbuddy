
var app = angular.module('registerform', ['customroutes','toaster']);

//added very later
app.service('CommonServices',function($http){

        this.saveData=function(DataToInsert,urlToSend){
        return $http({
            method  : "POST",
            data    : DataToInsert,
            url     : urlToSend,
           
        });

        };

        this.getData=function(urlToGet){
          return $http.get(urlToGet);
          
          };
        this.putData=function(urlToPut,dataToPut,whatToPut){
          console.log(dataToPut);
          return $http.put(urlToPut,{'dataToPut':dataToPut,'toUpdate':whatToPut});
        }
    });


app.controller('paymentPlanStory',function($scope,$rootScope,$http,toaster,CommonServices,$location){
 $scope.planid={};
      CommonServices.getData('paymentplan').
        success(function(dataResponse){
            if(dataResponse.success){
              $scope.paymentStory=dataResponse.success;
            }
            else if(dataResponse.fail){
              $scope.paymentStory='noRecords';
            }
            else {
              $scope.paymentStory='error';
            }
        }).error(function(dataResponse){
              $scope.paymentStory='error';
        });

      $scope.byThisPlan=function(plancode){

       
        
         console.log(plancode.byPlan);
        CommonServices.saveData(plancode,'buyplan').
            success(function(response){
              console.log(response);
            }).
            error(function(response){
               console.log(response);
            })

        $location.path('/makePaymentForThisJob');
      }



});

app.controller('catchDetail',function($scope,$http,toaster,CommonServices){


$scope.perupdate={};

 CommonServices.getData('profile').success(function(profiledetail){
      if(profiledetail.success){
      $scope.userDetail=profiledetail.success;
     
    

     angular.forEach(profiledetail.success,function(key,value){

      $scope.perupdate= {
                  firstname: key.firstname,
                  lastname : key.lastname,
                  phone    : key.phone,
                  location : key.location,
                  companyname :key.companyname,
                  domain   :key.domain 
              };

 
     });
      

     
      }
      else if(profiledetail.fail){
        toaster.pop('error','',profiledetail.fail);
      }
      else {
        toaster.pop('error','','We Found Some Difficulties.Please Contact Support.'); 
      }
    }).error();


 $scope.updatepersonel=function(){
 
  CommonServices.putData('updateData',$scope.perupdate,'perdata').success(function(updateresonse){
    if(updateresonse.success){
     
      toaster.pop('success','','Updated');
    }
    else if(updateresonse.fail){
      toaster.pop('error','','Fail To Update Try Again.');
    }
  }).error(function(updateresonse){
     toaster.pop('error','','Fail To Update Contact Support.');
  });
 };


 $scope.updateprofdetail=function(){

   CommonServices.putData('updateData',$scope.perupdate,'profDetail').success(function(updateresonse){
    if(updateresonse.success){
     
      toaster.pop('success','','Updated');
    }
    else if(updateresonse.fail){
      toaster.pop('error','','Fail To Update Try Again.');
    }
  }).error(function(updateresonse){
     toaster.pop('error','','Fail To Update Contact Support.');
  });

 };

});





app.controller('categoryrelated',function($scope,$http,toaster,CommonServices){
	
//check for payment plan

//get payment done or not if not then show me plan 
  

    CommonServices.getData('miniStatementJobPayment').success(function(dataResponse){
      console.log(dataResponse);

      if(dataResponse.AllGood){
        $scope.planCurrentStatus=dataResponse.AllGood;
        $scope.miniStateMentResponse='Yes';
     
      }
     else if(dataResponse.timeOver){
        $scope.planCurrentStatus=dataResponse.timeOver;
        $scope.miniStateMentResponse='No';
      }
      else if(dataResponse.jobOver){
        $scope.planCurrentStatus=dataResponse.jobOver;
        $scope.miniStateMentResponse='No';
      }
      else if(dataResponse.TimeOver){
         $scope.planCurrentStatus=dataResponse.TimeOver;
         $scope.miniStateMentResponse='No';

      }
      else if(dataResponse.Fail){
        $scope.planCurrentStatus={"planBookedOn":"NA","validTill":"NA","jobLeft":0,"message":"Your Did Not Posted Any Job."};
      }

    }).error(function(dataResponse){
      toaster.pop('error','','Http Error');
    });


    CommonServices.getData('knwCurrentStatysOfPaymentPlan').success(
      function(dataResponse){
        
        if(dataResponse.fail){
          $scope.paymentStatus='NotDone';
        }
        else
          if(dataResponse.success){
       
            $scope.paymentStatus='Done';
        }
        
        else{
          $scope.paymentStatus='Error';
        }

      }).error(function(dataResponse){
        console.log('HTTP ERROR');
      });

CommonServices.getData('categorylist').
          success(function(getDataResponse){
               
               toaster.pop("Warning","","Getting Category Lists");
              
                $scope.categorylist=getDataResponse;
              
              }).error(function(categorylist){
                toaster.pop("Warning","","Getting Category Lists");
                console.log(categorylist);
              });

CommonServices.getData('getData').
          success(function(getDataResponse){
               
              if(getDataResponse.success){

                 $scope.jobposted=getDataResponse.success;
               
              }
              else if(getDataResponse.fail){
                toaster.pop("error","","No Job Posted By You.");
              
                $scope.jobposted=[];
              
              }
              else {
                toaster.pop("Warning","","Contact Support");
              
                $scope.categorylist=getDataResponse.error;
              
              }

               
              }).error(function(categorylist){

                toaster.pop("Warning","","Getting Category Lists");
                console.log(categorylist);

              });


 

$scope.addCategory=function(){

	console.log($scope.newcategory);

	$http({
		method:"post",
		url:"/intern/admin-panel/public/createCategoryforJob",
		data:$scope.newcategory
	}).success(function(returncatugrydata){
		 if(returncatugrydata.fail){
     
       	angular.forEach(returncatugrydata.fail,function(key,value){
       		toaster.pop("error","",key);
       	});
       }
       else if(returncatugrydata.success){
       		angular.forEach(returncatugrydata.success,function(key,value){
       		toaster.pop("success","",key);
       	});
       }
       else if(returncatugrydata.error){
      
       	angular.forEach(returncatugrydata.error,function(key,value){
       		toaster.pop("error","",key);
       	});

       }else if(returncatugrydata.dataintigrity){
      		 console.log(returncatugrydata)
       			angular.forEach(returncatugrydata.dataintigrity,function(key,value){
       			toaster.pop("Warning","",key);
       	});	
       		}

       	else {
			console.log(returncatugrydata)
       		toaster.pop("Warning","","Contact Support");
       
       	}  

	}).error(function(returncatugrydata){
		console.log(returncatugrydata);
	});

	}
  $scope.activedeactivecatJob=function(activitytodo,idtodo){

    CommonServices.saveData({"id":idtodo,"status":activitytodo,"toggleto":"togglejobs"},'/intern/admin-panel/public/activedeactive').

    success(function(toggleresponse){

      if(toggleresponse.success){
     
          toaster.pop("success","",toggleresponse.success);
       
   }
   else if(toggleresponse.fail){

        toaster.pop("error","",toggleresponse.fail);
       
    
   }

    }).error(function(response){
    toaster.pop("error","","Some Error Occured Contact Support");

    });
   
  }


  $scope.activedeactivecat=function(activitytodo,idtodo){

    $http({
      
      url    :'/intern/admin-panel/public/activedeactive',
      method :'POST',
      data   :{"id":idtodo,"status":activitytodo,"toggleto":"togglecategory"},
    }).success(function(toggleresponse){

      if(toggleresponse.success){
     
          toaster.pop("success","",toggleresponse);
       
   }
   else if(toggleresponse.fail){

        toaster.pop("error","",toggleresponse);
       
    
   }

    }).error(function(response){
    toaster.pop("error","","Some Error Occured Contact Support");

    });
   
  }


/*===============================Post Job===================*/
$scope.free={};
 $scope.postfreejob=function(){
  //console.log($scope.free);
  CommonServices.saveData($scope.free,'postInternship').
          success(function(getDataResponse){
               
               toaster.pop("Warning","","Getting Category Lists");              
          if(getDataResponse.success){
          
          angular.forEach(getDataResponse.success,function(freekey,freevalue){
          
          toaster.pop("success","",freekey);
        
        });
        
        }
        else if(getDataResponse.fail){

          angular.forEach(getDataResponse.fail,function(key,value){
          toaster.pop("error","",key);
        });
        }
              
              }).error(function(getDataResponse){
                toaster.pop("Warning","","Contact Support");
                console.log(getDataResponse);
              });
    
  }



});

app.controller('freejobpost',function($scope,$http,toaster){


  $http({
    method  :"GET",
    url     :"/intern/admin-panel/public/listactivecategory"

  }).success(function(categorylist){

    $scope.activecategory=categorylist;

  }).error(function(categorylist){

    console.log("500: some error contact support");

  });

 


  
});
app.controller('registereduser',function($scope,$http,toaster){

    $http({
      method    :"GET",
      url       :"/intern/admin-panel/public/getregistereduser"

    }).success(function(responseuserlist){
   
    if(responseuserlist.success){
      console.log('hi');
         $scope.userlist=responseuserlist;
    }else if(responseuserlist.fail){
         toaster.pop('error',responseuserlist.fail);
   
    }

    }).error(function(responseuserlist){
      console.log(responseuserlist);
    });


    $scope.viewdetailofuser= function(userid){
      console.log(userid);
      $http({
        method  :"GET",
        data    :userid,
        url     :"/intern/admin-panel/public/getregistereduser"
      }).success(function(useridresponse){
        $scope.userdetaillist=useridresponse;
        console.log(useridresponse);

      }).error(function(useridresponse){
        console.log('error logged here');
      });
    }

    $scope.activedeactivecat=function(activitytodo,idtodo){

    $http({
      
      url    :'/intern/admin-panel/public/activedeactive',
      method :'POST',
      data   :{"id":idtodo,"status":activitytodo,"toggleto":"toggleregisteruser"},
    }).success(function(toggleresponse){

      if(toggleresponse.success){
     
          toaster.pop("success","",toggleresponse);
       
   }
   else if(toggleresponse.fail){

        toaster.pop("error","",toggleresponse);
       
    
   }

    }).error(function(response){
    toaster.pop("error","","Some Error Occured Contact Support");

    });
   
  }

  });

app.controller('listpostedjob',function($scope,$http,toaster){
  $http({
    url  :"/intern/admin-panel/public/listjobposted",
    method :"GET"
  }).success(function(listjob){
    $scope.postedjobs=listjob;
  }).error(function(listjob){

  });

  

    $scope.viewdetailofuser= function(userid){
      console.log(userid);
      $http({
        method  :"GET",
        data    :userid,
        url     :"/intern/admin-panel/public/getregistereduser"
      }).success(function(useridresponse){
        $scope.userdetaillist=useridresponse;
        console.log(useridresponse);

      }).error(function(useridresponse){
        console.log('error logged here');
      });
    }


 $scope.viewmoredetail= function(jobuserid){
      //console.log(jobuserid);
      $http({
        method  :"GET",
        data    :jobuserid,
        url     :"/intern/admin-panel/public/listjobposted"
      }).success(function(useridresponsedetail){
        $scope.userinformation=useridresponsedetail;
        console.log($scope.userinformation);

      }).error(function(useridresponsedetail){
        console.log('error logged here');
      });
    }




   $scope.activedeactivecat=function(activitytodo,idtodo){

    $http({
      
      url    :'/intern/admin-panel/public/activedeactive',
      method :'POST',
      data   :{"id":idtodo,"status":activitytodo,"toggleto":"togglejobs"},
    }).success(function(toggleresponse){

      if(toggleresponse.success){
     
          toaster.pop("success","",toggleresponse);
       
   }
   else if(toggleresponse.fail){

        toaster.pop("error","",toggleresponse);
       
    
   }

    }).error(function(response){
    toaster.pop("error","","Some Error Occured Contact Support");

    });
   
  }
});