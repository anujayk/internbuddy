
var myroutes = angular.module('customroutes', ['ngRoute']);

myroutes .config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: 'templ/dashboard.html',
       /* controller: 'categoryrelated'*/
      }).when('/dashboard', {
        templateUrl: 'templ/dashboard.html',
        /*controller: 'categoryrelated'*/
      }).
      when('/postJob', {
        templateUrl: 'templ/postjob.html',
      /*  controller: 'ShowOrdersController'*/
      }).
      when('/profile',{
        templateUrl:'templ/profile.html'
      }
        ).
      when('/buyPlan',{
        templateUrl:'templ/buyplan.html'
      }).
       when('/makePaymentForThisJob', {
        templateUrl: 'templ/paymentPage.html',
        controller: 'paymentPlanStory'
      }).
      otherwise({
        templateUrl:'templ/viewapplicants.html' // need to create later
      });
  }]);
