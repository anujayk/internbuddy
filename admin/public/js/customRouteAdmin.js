var myroutes = angular.module('customroutesAdmin', ['ngRoute']);

myroutes .config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: 'templ/admin/dashboard.html',
        controller: 'adminBasicController'
      }).when('/dashboard', {
        templateUrl: 'templ/admin/dashboard.html',
         controller: 'adminBasicController'

      }).
      /*when('/postJob', {
        templateUrl: 'templ/postjob.html',
      
      }).
      when('/profile',{
        templateUrl:'templ/profile.html'
      }
        ).
      when('/buyPlan',{
        templateUrl:'templ/buyplan.html'
      }).
       when('/makePaymentForThisJob', {
        templateUrl: 'templ/paymentPage.html',
        controller: 'paymentPlanStory'
      }).*/
      otherwise({
        templateUrl:'templ/admin/dashboard.html' // need to create later
      });
  }]);
