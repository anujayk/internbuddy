var customApp=angular.module('internapp',['ngRoute','toaster']);


customApp.service('CommonServices',function($http){

        this.saveData=function(DataToInsert,urlToSend){
        return $http({
            method  : "POST",
            data    : DataToInsert,
            url     : urlToSend,
           
        });

        };

        this.getData=function(urlToGet){
        	return $http.get(urlToGet);
        };
    });


customApp.controller('mysearchfilter',["$scope","$rootScope","CommonServices","toaster",function($scope,$rootScope,CommonServices,toaster){
             $rootScope.jobsFound={};
      $scope.mysearchattempt=function(eventP){

        eventP.preventDefault();
      //  console.log($scope.search);
      CommonServices.saveData($scope.search,'mysearchbox').
        success(function(responseData){
          if(responseData.Success){
            $rootScope.jobsFound=responseData.Success;
           $('#myJobs').foundation('reveal', 'open');
          }
          else if(responseData.Fail){
            toaster.pop('warning','','No Such Jobs Found');
          }
          else {
            toaster.pop('error','','Contact Support');
          }
          
        }).
        error(function(responseData){
          toaster.pop('error','','Http Error.');
        });
      }
      $scope.notLoggedIn=function(){
        toaster.pop('error','','You Need To Register Before Apply.');
      }
   
}]);

customApp.controller('CommonController',function($scope,CommonServices,$window,toaster){

	$scope.forgotpassword=function(){
    console.log('hi');
    $('.loginBox').addClass('animated bounceOutLeft');
    $('.forgotPasswordBox').addClass('animated bounceInLeft');
  }

  $scope.neverMind=function(){
      $('.forgotPasswordBox').addClass('animated bounceOutLeft');
      $('.loginBox').addClass('animated bounceOutIn');
  }
  $scope.subscribe=function(){
    console.log($scope.sEmail);
      CommonServices.saveData($scope.sEmail,'emails').
            success(function(response){
              if(response.Success){
            toaster.pop('success','','Thanx For Subscribing With Us.');
            }
            else if(response.Fail){
            toaster.pop('error','','Please Try Again.');
            }
            else {
             toaster.pop('error','','Contact Support.'); 
            }
          
            }).
            error(function(response){
              toaster.pop('error','','Http Error.');
            });
    /*if(response=='Success')*/
    
  }
  $scope.loginfun=function(e){

		 e.preventDefault(); 
		var urlToSend="login";
		CommonServices.saveData($scope.login,urlToSend).
			success(function(responselogin){
				//console.log(response.fail);

				if(responselogin.fail){
			 toaster.pop("error","","Email Password Missmatched");
				}
				else if(responselogin.success){
				 toaster.pop("success","","User Verified");
				$window.location.href = 'dashboard';
//				$location.path('dashboard');

				}
				else if(responselogin.fail){
					toaster.pop('error','','Wrong Email & Password Combination.');
					console.log(responselogin);
				}	
			});
		
	}
	$scope.userdata=function(e){
  	    e.preventDefault(); 
                
        if($scope.registeruser.email!==$scope.registeruser.confirmemail){
          toaster.pop('error','','Email And Confirmation Email SHould Be Same');
          return false;
        }

        CommonServices.saveData($scope.registeruser,'/intern/admin-panel/public/register').
        success(function(response) {

       if(response.fail){
       	console.log(response)
       	angular.forEach(response.fail,function(key,value){
       		toaster.pop("error","",value);
       	});
       }
       else if(response.success){
       		angular.forEach(response.success,function(key,value){
       		toaster.pop("success","",key);
       	});
       }
       else if(response.error){
      
       	angular.forEach(response.error,function(key,value){
       		toaster.pop("Warning","",key);
       	});

       }else if(response.dataintigrity){
      		 console.log(response)
       			angular.forEach(response.dataintigrity,function(key,value){
       			toaster.pop("Warning","",key);
       	});	
       		}

       	else {
			console.log(response)
       		toaster.pop("Warning","","Contact Support");
       
       	}    
	    }).error(function(response){
    	console.log(response)
    });
    };


});
