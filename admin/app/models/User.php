<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');


	public function insertData($tableName,$dataArray)
	{
		$record=DB::table($tableName)->insert($dataArray);
		if($record){
			return True;
		}elseif (!$record) {
			return False;
		}
		else{
			return 'fail';
		}

	}
	public function checkExistance($tableName,$dataMatchArray)
	{
		$record=DB::table($tableName)->where($dataMatchArray)->get();
		if(sizeof($record)>0){
			return True;
		}elseif (sizeof($record)) {
			return False;
		}
	}

	public function getData($tableName,$dataToSearch)
	{
		if($dataToSearch){

		$result=DB::table($tableName)->where(function($query) use ($dataToSearch){
			foreach($dataToSearch as $key=>$value){
				$query->where($key,$value);
			}
		});
		}elseif($dataToSearch==False){
			$result=DB::table($tableName);
		}
		return $result;
		
	}

	public function updateData($tableName,$dataToUpdateArray,$updateKey)
	{
		
		$recordToUpdate=DB::table($tableName)->where($updateKey)->update($dataToUpdateArray);
		if($recordToUpdate){
			return True;
		}
		elseif(!$recordToUpdate){
			return False;
		}
	}
}
