<?php

class HomeController extends BaseController {


	public $objectData;
	public $postJob=array();

	function __construct(){
		$this->objectData=new User;
		$this->postJob =array(
			/*'employerid' 			   	=> 'required|numeric|min:2',*/
			'title' 					=> 'required|min:2',
			'categoryid' 				=> 'required|numeric|min:2',
			'description' 				=> 'required|min:20',
			'requirements' 				=> 'required|min:5',
			
			
			'city' 						=> 'required|min:5',
			'state' 					=> 'required|min:2',
			'hours' 					=> 'required|in:fulltime,parttime',
			'compensation' 				=> 'required|in:paid,unpaid',
			'position_required' 		=> 'required|numeric|min:1',
			/*'from_date' 				=> 'required|date_format:Y-m-d',
			'till_date' 				=> 'required|date_format:Y-m-d',
			'closing_on'				=> 'required|date_format:Y-m-d',*/
			'type' 						=> 'required|in:planning,urgent,major,critical'
			);
	}


	public function showWelcome()
	{
		return View::make('hello');
	}

	public function home()
	{
		return View::make('index')->with(array('user','hi'));
	}


	public function logout()
	{
		Auth::logout(); // log the user out of our application
		//Session::forget();
		Session::flush();
		return Redirect::to('/'); // redirect the user to the login screen

	}

	public function dashboard()
	{
		if (Auth::check())
		{
		    echo 'hello'.Session::get('username');
		}

		else{
			return '/';
		}
	}

	public function listCategory()
	{
		$categorylist=$this->objectData->getData('tbl_category',null)->get();
		if(sizeof($categorylist)>0){
			$response=array("success"=>$categorylist);
		}
		else if(sizeof($categorylist)==0){
			$response=array("fail"=>"No Records Found");
		}
		else{
			$response=array("error"=>"contact support");
		}
		return json_encode($response);
	}

	public function postInternship()
	{
		$data=Input::all();

		 $userid=array(
		 	'employerid' =>Session::get('userid')
		 	);
		 $data=array_merge($userid,$data);
		/* print_r($data);
		die;
		 */
		$validation=Validator::make($data,$this->postJob);
		if($validation->fails()){
			return json_encode(array("fail"=>$validation->messages()));
		}
		elseif ($validation->passes()) {


			$dataAgainstUpdate =array(
				'employer_id'  		=> Session::get('userid'),
				'payment_success'	=>'yes',
				'currentPlan'		=>'yes'	
				);

			$postInternshipJob=$this->objectData->insertData('tbl_internship_posted',$data);

			$updateInPlan=DB::table('employer_payment_plan')->where(function($query) use($dataAgainstUpdate){

				foreach ($dataAgainstUpdate as $key => $value) {
					$query->where($key,$value);
				}
			})->increment('jobPosted');
			
			if($postInternshipJob==True){
				return json_encode(array("success"=>array('success'=>'Job Posted')));

			}elseif ($postInternshipJob==False) {

				return json_encode(array("fail"=>array('Fail'=>'Fail To Post Job, Try After Some Time')));
				
			}else{
				return json_encode(array("error"=>array('error'=>'We Found Some Difficulties Constact Support')));
			}

			
		}
	}

	public function profile()
	{
		if(Auth::check()){
			
			$userdetail=$this->objectData->getData('tbl_registered_user_employer_detail',array('id'=>Session::get('userid')))->get();
			
			if(sizeof($userdetail)>0){
				return json_encode(array('success'=>$userdetail));
			}
			else{
				return json_encode(array('fail'=>'We Are Unable To Find Records Contact Support.'));
			}
		}
	}
	public function updateData()
	{
		$data=Input::all();
	
		if($data['toUpdate']=='profDetail'){
			$tableName="tbl_registered_user_employer_detail";
		}
		elseif($data['toUpdate']=='perdata'){
			$tableName="tbl_registered_user_employer_detail";
		}
		//echo Session::get('userid');
		$updataRecord=$this->objectData->updateData($tableName,$data['dataToPut'],array('id'=>Session::get('userid')));
		

		if($updataRecord){
			return json_encode(array("success"=>array('success'=>'Records Updated')));
		}
		elseif($updataRecord==false){
			return json_encode(array('fail'=>'Fail To Update Try After Some TIme'));
		}
		else {
			return json_encode(array('error'=>'Contact Support'));	
		}
	}

	public function getData()
	{
		
		$responseGet=$this->objectData->getData('tbl_internship_posted',array('employerid'=>Session::get('userid')))->get();

		if(sizeof($responseGet)>0){
			return json_encode(array('success'=>$responseGet));
		}
		else if(sizeof($responseGet)==0){
			return json_encode(array('fail'=>'No Job Posted'));
			
		}
		else{
			return json_encode(array('error'=>'Try After Some Time,Contact Support'));
	
		}
	}

}
