<?php

class internship extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//

		$result=DB::table('tbl_internship_posted')->get();

		$result=DB::table('tbl_internship_posted')
            ->join('tbl_registered_user_employer_detail', 'tbl_internship_posted.employerid', '=', 'tbl_registered_user_employer_detail.id')
            ->select('*','tbl_internship_posted.status')
            ->get();

		$queries = DB::getQueryLog(); // gets a log of all executed queries

		if(sizeof($result)==0){
			return Response::json(array('Fail'=>'No Internship POsted.'));
		}
		else if(sizeof($result)>0){
			
			return Response::json(array('Success'=>'Success','data'=>$result));
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
