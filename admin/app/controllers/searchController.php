<?php

class searchController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
		public $userObject;

	 function __construct(){
	 	$this->userObject=new User;
	 }

	public function index()
	{
		//

		

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$dataToSearch=Input::all();

		$result=DB::table('tbl_internship_posted')->where(function($query) use($dataToSearch){
			foreach ($dataToSearch as $key => $value) {
				$query->where($key,'Like','%'.$value.'%');
			}
		})->get();
		//echo 'hi';
		if(sizeof($result)>0){
			foreach ($result as $key => $value) {
				$resultData=$this->userObject->getData('tbl_registered_user_employer_detail',array('id'=>$value->employerid))->get();
				foreach ($resultData as $k => $v) {
					$result[$key]->companyname=$v->companyname;
				}
			}
			return json_encode(array('Success'=>$result));
		}
		else if(sizeof($result)==0){
			return json_encode(array('Fail'  =>'No result Found'));
		}
		else {
			return json_encode(array('Error' =>'Contact Support'));
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
