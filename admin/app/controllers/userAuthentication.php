<?php

class userAuthentication extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public $userObject;

	function __construct(){

		$this->userObject=new User;

	}
	public function index()
	{
		//
	}

	public function login()
	{
		$rules=array(
			'email'=>"required",
			'password'=>"required"
			);
		$data=Input::all();
	
		$validator = Validator::make($data,$rules);
		
		if($validator->passes())
		{
		
			if(Auth::attempt($data)){


				
				$searchData=array('email'=>$data['email']);
				$getUserDetail=$this->userObject->getData('tbl_registered_user_employer_detail',$searchData)->get();
				
				Session::put('useremail',$data['email']);
				Session::put('userid',$getUserDetail[0]->id);
				Session::put('username',$getUserDetail[0]->firstname);
				Session::put('userrole',Auth::user()->role);


				return json_encode(array('success'=>$data),JSON_PRETTY_PRINT);

			}
			else{
					$_SESSION['user'] =$data['email'];
			return json_encode(array('fail'=>'user pwd missmatch'));
		}
		//return Redirect::back()->withInput()->with('failure','username or password is invalid!');
		}
		
		else
		{
		return json_encode(array('fail'=>$validator->messages()));
		 
		}
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */


	public function test()
	{
		$searchData=array('email'=>'anujay123@gmail.com');
		$getUserDetail=$this->userObject->getData('tbl_registered_user_employer_detail',$searchData)->get();
				
				/*echo json_encode($getUserDetail,JSON_PRETTY_PRINT);
				die;*/
				$data1=Session::put('username','anujay123@gmail.com');
				$data2=Session::put('userid',$getUserDetail[0]->id);

				echo Session::get('username');
				echo Session::get('userid');
				//return json_encode(array('success'=>$data),JSON_PRETTY_PRINT);
	}
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
