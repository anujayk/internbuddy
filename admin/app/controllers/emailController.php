<?php

class emailController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public $userObject;

	public function __construct()
	{
		$this->userObject=new User;
	}
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$data=Input::all();
		
		if(!isset($data)){

			return Response::json(array('Fail'=>'Invalid Params'),405);
		}
		else {

			$dataArray=array(
				'email'      =>$data['subsEmail'],
				'option'     =>'yes',
				'createdat'  =>date("Y-m-d H:i:s",strtotime("now"))
				);

			$this->userObject->insertData('subscribction',$dataArray);

			return Response::json(array('Success'=>array(
													'size'  =>sizeof($data),
													'data'  =>$data
											)),201);
		}
	}


  public function resetPassword()
  {
  	$email=Input::all();


  	$emailExists=DB::table('users')->where('email',trim($email['email']))->get();

  	if(sizeof($emailExists)==0){
  		return Response::json(array('Fail' =>'No Such Email Exists.'));
  	}
  	else {
  		$token=md5(uniqid(mt_rand(), true));
  		$emailUpdate=DB::table('users')->where('email',$email['email'])
  						->update(array('password'=>$token,'emailchangecode'=>$token));
		return Response::json(array('Success' =>array(
										'token'      =>$token,
										'returnUrl'  =>'mytreturn Url'
											)));			
  	}
  }
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
