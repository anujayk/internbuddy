<?php

class MiniStatementPaymentJob extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public $userObject;

	 function __construct(){


	 	$this->userObject=new User;
	 }


	public function index()
	{
		//


		$userid=Session::get('userid');

		//$userid=13;
		$searchKey=array(
			'employer_id'       => $userid,
			'currentPlan'      =>'Yes',
			'payment_success'   =>'Yes'
			);
		$miniStatement=$this->userObject->getData('employer_payment_plan',$searchKey)->get();


		if(sizeof($miniStatement)==0){
			return json_encode(array(
				'Fail' =>'No Records Found'
				));
		    }
		 elseif (sizeof($miniStatement)==1) {
		    	
		 	
		 	   //	echo $miniStatement[0]->plan_selected;

		 	   	$keyToSearch=array(
		 	   		'id'      => $miniStatement[0]->plan_selected
		 	   		);
		 	   	
		 	   	$planTrack=$this->userObject->getData('payment_plan',$keyToSearch)->get();
 		if(sizeof($planTrack)==0){
		 	   			return json_encode(array('Fail' => 'No Such Payment Plan In Plan List'));
		 	   		}
		 	   		else if(sizeof($planTrack)==1){

		 	   
		 	  
		 	   			$startDate=date('Y-m-d',strtotime($miniStatement[0]->updated_at));
		 	   			$TodayDate=date('Y-m-d',strtotime("now"));

		 	   			

		 	   			if(strtotime($TodayDate) >= strtotime($startDate))
			 	   			{

			 	   			   if($planTrack[0]->for_duration=='month'){
			 	   			   	$validTill = strtotime(date("Y-m-d", strtotime($startDate)) . " +1 month");
			 	   			   }
			 	   			   else if($planTrack[0]->for_duration=='year'){
			 	   			   	$validTill = strtotime(date("Y-m-d", strtotime($startDate)) . " +1 year");
			 	   			   }
			 	   			   else {
			 	   			   	$validTill=Null;
			 	   			   }
			 	   			  

							$dateLeft=round(($validTill - time()) / 86400);

							
			 	   			}
			 	   			else {
			 	   				$dateLeft=0;
			 	   				
			 	   			}

			 	   		

		 	   		if($dateLeft<0){
		 	   			return json_encode(array(
		 	   				'timeOver' =>array(
		 	   					'planBookedOn'  =>$startDate,
		 	   					'validTill'     =>date('Y-m-d',$validTill),
		 	   					'jobLeft'	    =>($planTrack[0]->job_for_cost - $miniStatement[0]->jobPosted),
		 	   					'message' 		=>'Your Plan Period Is Over.'
		 	   						)
		 	   					)
		 	   				);
		 	   			die;
		 	   		}


		 	   	if($miniStatement[0]->jobPosted < $planTrack[0]->job_for_cost){

		 	   		



		 	   		return json_encode(array('AllGood' =>array(
		 	   			'jobPosted'         =>$miniStatement[0]->jobPosted,
		 	   			'jobLeft'           =>($planTrack[0]->job_for_cost - $miniStatement[0]->jobPosted),
		 	   			'BookedOn'          =>$startDate,
		 	   			'validTill'			=>date('Y-m-d',$validTill),
		 	   			'tomeLeftToExpire'  =>$dateLeft,
		 	   			'message'           =>'All Going Good'
		 	   			)));
		 	   	}		
		 	   	elseif($miniStatement[0]->jobPosted == $planTrack[0]->job_for_cost){

		 	   		return json_encode(array(
		 	   			'jobOver' =>array(
		 	   			 	'jobPosted'         =>$miniStatement[0]->jobPosted,
			 	   			'jobLeft'           =>($planTrack[0]->job_for_cost - $miniStatement[0]->jobPosted),
			 	   			'BookedOn'          =>$startDate,
			 	   			'validTill'			=>date('Y-m-d',$validTill),
			 	   			'tomeLeftToExpire'  =>$dateLeft,
		 	   				'message'   =>'You Plan Quote Finished,Renew Your Plan.'
		 	   				)
		 	   			)
		 	   		);
		 	   	}
		 	   	else if(strtotime("now") > $validTill){
		 	   		return json_encode(array('TimeOver'  =>array(
		 	   			'jobPosted'         =>$miniStatement[0]->jobPosted,
		 	   			'jobLeft'           =>($planTrack[0]->job_for_cost - $miniStatement[0]->jobPosted),
		 	   			'BookedOn'          =>$startDate,
		 	   			'validTill'			=>date('Y-m-d',$validTill),
		 	   			'tomeLeftToExpire'  =>$dateLeft,
		 	   			'message'=>'Job Duration Over ,Buy Again.')
		 	   		)
		 	   		);
		 	   	}

		 	   		}
		 	   		else {
		 	   			return json_encode(array('Error'  => 'Contact Support'));
		 	   		}

		    	/*return json_encode(array(
		    		'Success' => $miniStatement
		    		));*/
		    }
		    else {
		    	return json_encode(array(
		    		'Error'   =>$miniStatement
		    		));
		    }   


	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
