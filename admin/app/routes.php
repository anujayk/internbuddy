<?php


Route::get('test','userAuthentication@test');

/*=======================Login Related========================*/
Route::get('/',array('uses'=>'HomeController@home'));
Route::post('/login',array('uses'=>'userAuthentication@login'));
Route::get('/logout', array('uses' => 'HomeController@logout'));
Route::get('/employers',function(){
	return View::make('employers');
});
Route::get('/profile','HomeController@profile');
Route::put('/updateData','HomeController@updateData');

/*===========================Payment Related========================*/

Route::get('/paymentType/{id}','PaymentController@show');

/*=============================Dashboard=============*/


Route::get('/categorylist','HomeController@listCategory');

/*========================Post Job==================*/

Route::post('/postInternship','HomeController@postInternship');
Route::get('/getData','HomeController@getData');

Route::resource('internship','internship');   //all internship posted POST
Route::resource('applicants','applicant');   //all those who applied to job

Route::get('dashboard',function(){
if(Auth::check()){
	return View::make('dashboard');
}
else{
	return Redirect::to('/');
}	
});


/*===============================================================
                          Payment Plan
   ==================================================*/


Route::resource('paymentplan','paymentController');



/*===============================================================
                          User Payment Related
   ==================================================*/



Route::resource('paymentdetail','EmployerPaymentController');
Route::resource('buyplan','EmployerPaymentController@buyplan');
Route::resource('knwCurrentStatysOfPaymentPlan','EmployerPaymentController@knwCurrentStatysOfPaymentPlan');
/*==================================================================================
					Get How MUhc Job Posted And How Much Left For CUrrenct Plan
  ==================================================================================
  */

Route::resource('miniStatementJobPayment','MiniStatementPaymentJob');

Route::resource('mysearchbox','searchController');


Route::resource('emails','emailController');

Route::post('change','emailController@resetPassword');


