<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/custom.css">
     <link rel="stylesheet" type="text/css" href="css/hint.css">
    <link rel="stylesheet" type="text/css" href="css/toaster.css"/>
   
  </head>
  <body ng-app="registerform">

    <p ng -controller="catchDetail" ng-model="userid."<?php echo Session::get('userid');?>""></p>
  <div id="left-section">
    <div class="header">
      <a href="" class="logo"><?php echo ucfirst(Session::get('username')); ?></a>
      <div class="user-info">
        <a href="" class="dropdown-toggle" data-toggle="dropdown"><img src="img/avatar.png"><i class="arrow_box"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#profile">Profile</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="logout">Logout</a></li>
          </ul>
      </div>


      <div class="menu">
        <ul class="nav">
          <li><a href="#" class="active"><i class="glyphicon glyphicon-envelope"></i><span>Dashboard</span></a></li>
          <li><a href="#postJob"><i class="glyphicon glyphicon-briefcase"></i><span>Post Job</span></a></li>
          <li><a href="#viewApplicants"><i class="glyphicon glyphicon-user"></i><span>View Applicants</span></a></li>
          <li><a href=""><i class="glyphicon glyphicon-cog"></i><span>Settings</span></a></li>
          <li><a href=""><i class="glyphicon glyphicon-book"></i><span>Book</span></a></li>
        </ul>
      </div>
    </div>
  </div>
 
  <div id="right-section">
    <div class="header">
      <h5></h5>
    </div>
     <div class="main-data-container">
      
    
     
        <div ng-view></div>

    </div>
  </div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.0-beta.2/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.0-beta.2/angular-route.min.js"></script>

    <script type="text/javascript" src="js/toaster.js"></script>
    <?php if(Session::get('userrole')!='admin') {
      ?>
      <script type="text/javascript" src="js/customroutes.js"></script>
      <script type="text/javascript" src="js/custom.js"></script>
  
     <?php } else if(Session::get('userrole')=='admin'){?>
      <script type="text/javascript" src="js/customRouteAdmin.js"></script>
      <script type="text/javascript" src="js/customAdmin.js"></script>
  
        <?php }?>
    

  
  </body>
</html>