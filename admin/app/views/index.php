<!doctype html>
<html  lang="en" >
  <head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Internbuddy</title>
      <link rel="stylesheet" href="css/foundation.css" />
      <link rel="stylesheet" type="text/css" href="css/slippry.css"/>
      <link rel="stylesheet" type="text/css" href="css/customfront.css"/>
      <link rel="stylesheet" type="text/css" href="css/toaster.css"/>
      <link rel="stylesheet" type="text/css" href="css/ngAnimate.css">
      <link rel="stylesheet" type="text/css" href="https://raw.githubusercontent.com/daneden/animate.css/master/animate.css">
  </head>
  <body ng-app="internapp">
     <toaster-container toaster-options="{'time-out': 3000}"></toaster-container>
    <header   ng-controller="CommonController">
      <div class="row">
   <?php  
   if(Session::get('username')){

           echo '<div class="user-view">
                    <div class="user-view-sess">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="dashboard"><img src="img/avatar.png"><i class="arrow_box"></i></a>
                    </div>
                </div>    ';
      }
   ?>   

        <div class="emp-login">
          <img src="img/emp-button.png" data-reveal-id="myModal">
          <div id="myModal" class="reveal-modal medium login-data " data-reveal>
            <div class="form-container loginBox">
              <div class="row">
                <div class="large-5 columns">
                  <div class="login-container">
                    <h5 class="title">Login below with your email address.</h5>
                    <form ng-submit="loginfun($event)">
                    <label>
                      <input type="text" placeholder="email" ng-model="login.email">
                    </label>

                    <label>
                      <input type="password" placeholder="password" ng-model="login.password">
                    </label>

                    <label for="checkbox1"><input id="checkbox1" type="checkbox"><span class="check">Keep me login in on this computer</span></label>
                    <label><a href="" ng-click="forgotpassword()">Forgot Password?</a></label>
                    

                    <div class="button-holder" style="margin-top:15px;">
                      <button class="button tiny"><img src="img/read-button.jpg"></button>
                    </div>
                    </form>
                    <h6 class="or-border">
                      <span>OR</span>
                    </h6>
                  </div>
                </div>
              
               <div class="large-7 columns" >
                  <div class="sign-up-container" >
                    <h4 class="title">Not a member? </h4>
                     <form ng-submit="userdata($event)">

                        <label>
                          <input type="text" name="firstname" ng-model="registeruser.firstname" placeholder="Firstname" required/>
                        </label>

                        <label>
                          <input type="text" name="lastname" ng-model="registeruser.lastname" placeholder="Lastname" required/>
                        </label>

                        <label>
                          <input type="text" name="email" ng-model="registeruser.email" placeholder="E-mail Address" required/>
                        </label>

                        <label>
                          <input type="text" name="cemail" ng-model="registeruser.confirmemail" placeholder="Confirm E-mail Address" required/>
                        </label>

                        <label>
                          <input type="password" name="password" ng-model="registeruser.password" placeholder="Password" required/>
                        </label>

                        <label>
                          <input type="password" name="cpassword" ng-model="registeruser.confirmpassword" placeholder="Confirm Password" required/>
                        </label>

                        <span class="gender">Gender :</span>
                        <input type="radio" name="gender" ng-model="registeruser.gender" value="male" id="male" ><label for="male">Male</label>
                        <input type="radio" name="gender" value="female" ng-model="registeruser.gender" id="female"><label for="female">Female</label>
                        <br />
                        <input type="checkbox" required>I Aggree
                        <div class="button-holder">
                            <button class="button tiny" type="submit"><img src="img/sign-up.jpg" /></button>
                        </div>
                    </form>
                  </div>  
                </div> 
              </div>
            </div>
            <div class="form-container forgotPasswordBox hide">
              <div class="row">
                <div class="large-5 columns">
                  <div class="login-container">
                    <h5 class="title">Login below with your email address.</h5>
                    <form ng-submit="loginfun($event)">
                    <label>
                      <input type="text" placeholder="email" ng-model="login.email">
                    </label>

                    <label>
                      <input type="password" placeholder="password" ng-model="login.password">
                    </label>

                    <label for="checkbox1"><input id="checkbox1" type="checkbox"><span class="check">Keep me login in on this computer</span></label>
                    <label><a href="" ng-click="forgotpassword()">Forgot Password?</a></label>
                    

                    <div class="button-holder" style="margin-top:15px;">
                      <button class="button tiny"><img src="img/read-button.jpg"></button>
                    </div>
                    </form>
                    <h6 class="or-border">
                      <span>OR</span>
                    </h6>
                  </div>
                </div>
              
               <div class="large-7 columns" >
                  <div class="sign-up-container" >
                    <h4 class="title">Not a member? </h4>
                     <form ng-submit="userdata($event)">

                        <label>
                          <input type="text" name="firstname" ng-model="registeruser.firstname" placeholder="Firstname">
                        </label>

                        <label>
                          <input type="text" name="lastname" ng-model="registeruser.lastname" placeholder="Lastname">
                        </label>

                        <label>
                          <input type="text" name="email" ng-model="registeruser.email" placeholder="E-mail Address">
                        </label>

                        <label>
                          <input type="text" name="cemail" ng-model="registeruser.confirmemail" placeholder="Confirm E-mail Address">
                        </label>

                        <label>
                          <input type="password" name="password" ng-model="registeruser.password" placeholder="Password">
                        </label>

                        <label>
                          <input type="password" name="cpassword" ng-model="registeruser.confirmpassword" placeholder="Confirm Password">
                        </label>

                        <span class="gender">Gender :</span>
                        <input type="radio" name="gender" ng-model="registeruser.gender" value="male" id="male" ><label for="male">Male</label>
                        <input type="radio" name="gender" value="female" ng-model="registeruser.gender" id="female"><label for="female">Female</label>

                        <div class="button-holder">
                            <button class="button tiny" type="submit"><img src="img/sign-up.jpg" /></button>
                        </div>
                    </form>
                  </div>  
                </div> 
              </div>
            </div>
            <a class="close-reveal-modal">&#215;</a>
          </div>

        
        </div>
        <div class="large-12 columns">
          <img src="img/logo1.jpg" class="logo">
        </div>
      </div>
    </header>
    
    <div class="slider">
      <!-- <img src="img/slide.jpg"> -->

      <ul id="demo1">
        <li><a href="#slide1"><img src="img/slide.jpg" ></a></li>
        <li><a href="#slide2"><img src="img/slide1.jpg" ></a></li>
        <li><a href="#slide3"><img src="img/slide2.jpg" ></a></li>
      </ul>

      <span class="fst">Your One Stop</span>
      <span class="scnd">Internship Shop</span>

      <div class="caption" ng-controller="mysearchfilter">
  <form ng-submit="mysearchattempt($event)">      
        <div class="row">

          <div class="large-6 medium-6 columns padding-none">
              <span class="caption-span">I'm serach by</span>
                <div class="large-6 medium-6 columns">
                  <input ng-model="search.requirements" type="text" placeholder="Internship Keyword">
                </div>

                <div class="large-6 medium-6 columns">
                  <input ng-model="search.title" type="text" placeholder="Major">
                </div>
          </div>

          <div class="large-6 medium-6 columns padding-none">
            <span class="caption-span left-pad">Location</span>
            <div class="large-6 medium-6 columns">
              <input ng-model="search.city" type="text" placeholder="City, State, or Zip Code">
            </div>

            <div class="large-6 medium-6 columns last-button">
              <button class="button tiny orange-button width-button" type="submit" >
                  <img src="img/search.png" style="width:166px;" >
              </button>
            </div>
          </div>

        </div>
  </form>
       
      </div>
    </div>

    <div class="text-container">
      <div class="row">
        <div class="large-6 medium-6 columns">
          <div class="box-container">
            <div class="img-container">
              <img src="img/hot.png">
              <span class="hot">Hot internship</span>
            </div>

            <div class="text-area">
              <p>Generate hours worked and wages payable in a singhle click Run Generate hours worked and wages payable in a singhle click Run Generate hours worked and wages .</p>
              <button class="button orange-button"><img src="img/read-button.png"></button>
            </div>
          </div>
        </div>

        <div class="large-6 medium-6 columns">
          <div class="box-container">
            <div class="img-container">
              <img src="img/comp.png">
              <span class="comp">Companies</span>
            </div>

            <div class="text-area">
              <p>Generate hours worked and wages payable in a singhle click Run Generate hours worked and wages payable in a singhle click Run Generate hours worked and wages .</p>
              <button class="button orange-button"><img src="img/read-button.png"></button>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="large-6 medium-6 columns">
          <div class="box-container">
            <div class="img-container">
              <img src="img/blog.png">
              <span class="blog">Blog's post</span>
            </div>

            <div class="text-area">
              <p>Generate hours worked and wages payable in a singhle click Run Generate hours worked and wages payable in a singhle click Run Generate hours worked and wages .</p>
              <button class="button orange-button"><img src="img/read-button.png"></button>
            </div>
          </div>
        </div>

        <div class="large-6 medium-6 columns">
          <div class="box-container">
            <div class="img-container">
              <img src="img/interview.png">
              <span class="inter">interviews</span>
            </div>

            <div class="text-area">
              <p>Generate hours worked and wages payable in a singhle click Run Generate hours worked and wages payable in a singhle click Run Generate hours worked and wages .</p>
              <button class="button orange-button"><img src="img/read-button.png"></button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <footer>
      <div class="row">
        <div class="large-4 medium-4 columns">
          <div class="main">
            <h4 class="title">Contact manager</h4>
            <p>110 West Avenue<br>
            Miami Beach,FL33139<br>
            <span>+1-000-000-0000</span>
            info@internbuddy.com
            </p>
          </div>
        </div>

        <div class="large-4 medium-4 columns" ng-controller="CommonController">
          <div class="main">
            <h4 class="title">Subscribe newsletter</h4>
            <p>For getting our latest news &amp; updatea sign up our newsletter and get updates directly in your mail!!!
            </p>
            <form ng-submit="subscribe()">
            <input type="email" placeholder="Your Email Id" ng-model="sEmail.subsEmail"  required/>
            <button class="button orange-button" type="submit"><img src="img/sub-button.png"></button>
            </form>
          </div>
        </div>

        <div class="large-4 medium-4 columns">
          <div class="main last">
            <h4 class="title">Website Links</h4>
            <ul>
              <li><a href="">&raquo; Home</a></li>
              <li><a href="">&raquo; About Us</a></li>
              <li><a href="">&raquo; Blog</a></li>
              <li><a href="">&raquo; Help</a></li>
              <li><a href="">&raquo; Contact Us</a></li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
    <div ng-controller="mysearchfilter">
<div id="myJobs" class="reveal-modal xlarge"  data-reveal>
            <div class="form-container">
              <div class="row">
                <div class="large-12 columns">
               
                  <table>
  <thead>
    <tr>
      <th>SNo.</th>
      <th>Company Name</th>
      <th>Job Title</th>
      <th>Requirements</th>
      <th>Description</th>
      <th>Location</th>
      <th>Position</th>
      <th>Type</th>
      <th>Apply</th>
    </tr>
  </thead>
  <tbody>
    <tr ng-repeat="jobs in jobsFound">
      <td>{{$index+1}}</td>
      <td>{{jobs.companyname}}</td>
      <td>{{jobs.title | uppercase}}</td>
      <td>{{jobs.requirements | uppercase}}</td>
      <td>{{jobs.description | uppercase}}</td>
      <td>{{jobs.city | uppercase}},{{jobs.state | uppercase}}</td>
      <td>{{jobs.position_required | uppercase}}</td>
      <td>{{jobs.hours | uppercase}} , {{jobs.compensation | uppercase}}</td>
      <td ng-if="jobs.status=='active'"><button class="tiny button success" ng-click="notLoggedIn()">Apply</button></td>
      <td ng-if="jobs.status=='deactive'"><button class="tiny button alert disabled">Expired</button></td>
    </tr>
    
  </tbody>
</table>
                </div>
              
              
              </div>
            </div>
            <a class="close-reveal-modal">&#215;</a>
          </div>
          </div>
    <div class="footer-bottom">
      <div class="row">
        <div class="large-12 columns">
          <div class="large-6 columns">
            <span>2014 &copy; Internbuddy.com,All rights reserved.</span>
          </div>
          <div class="large-6 columns right-part">
            <span>Developed &amp; design by:Intellectual Boot</span>
          </div>
        </div>
      </div>
    </div>
    </div>

   <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
   
 <script src="js/foundation.min.js"></script>
 <script src="js/slippry.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.0rc1/angular.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0rc1/angular-route.min.js"></script>
 <script type="text/javascript" src="js/toaster.js"></script>
<!-- <script type="text/javascript" src="js/custom.js"></script> --!-->
 <script type="text/javascript" src="js/customangular.js"></script>
 
  <script>
      $(document).foundation();
    </script>
    
    <script>
      $(function() {
        var demo1 = $("#demo1").slippry({
          transition: 'fade',
          useCSS: true,
          speed: 1000,
          pause: 3000,
          auto: true,
          preload: 'visible'
        });

        $('.stop').click(function () {
          demo1.stopAuto();
        });

        $('.start').click(function () {
          demo1.startAuto();
        });

        $('.prev').click(function () {
          demo1.goToPrevSlide();
          return false;
        });
        $('.next').click(function () {
          demo1.goToNextSlide();
          return false;
        });
        $('.reset').click(function () {
          demo1.destroySlider();
          return false;
        });
        $('.reload').click(function () {
          demo1.reloadSlider();
          return false;
        });
        $('.init').click(function () {
          demo1 = $("#demo1").slippry();
          return false;
        });
      });
    </script>
  
  </body>
</html>
