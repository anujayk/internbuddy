<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	public $rules			=array();
	public $createCategory  =array();
	public $postJob         =array();
	public $objUser;

	 function __construct(){

		$this->rules=array(
				
				'firstname'	  			=>'required|alpha|min:2',
                'lastname'	  			=>'required|alpha|min:2',
                'email'	 	 		    =>'required|email|unique:users,email,'.Input::get('id').',id',
                'password'	  		    =>'required|between:6,12',
                'confirmpassword'		=>'required|between:6,12',
                'address'				=>'regex:/^[a-z0-9- ]+$/i|min:2',
                'city'					=>'alpha|min:2',
                'state'					=>'alpha|min:2|max:2',
                'zip'					=>'numeric|min:5|max:5',
                'phone'					=>'regex:/^\d{3}\-\d{3}\-\d{4}$/',
			);

		$this->createCategory=array(
			'display_title'				=> 'required|min:2',
			'name' 					    => 'required|min:4',
			'description' 				=> 'required|min:2',
			'title' 			        => 'required|min:2',
			);

		$this->postJob =array(
			'employerid' 			   	=> 'required|numeric|min:2',
			'title' 					=> 'required|min:2',
			'categoryid' 				=> 'required|numeric|min:2',
			'description' 				=> 'required|min:20',
			'requirements' 				=> 'required|min:5',
			
			
			'city' 						=> 'required|min:5',
			'state' 					=> 'required|min:5',
			'hours' 					=> 'required|in:fulltime,parttime',
			'compensation' 				=> 'required|in:paid,unpaid',
			'position_required' 		=> 'required|numeric|min:1',
			/*'from_date' 				=> 'required|date_format:Y-m-d',
			'till_date' 				=> 'required|date_format:Y-m-d',
			'closing_on'				=> 'required|date_format:Y-m-d',*/
			'type' 						=> 'required|in:planning,urgent,major,critical'
			);
		
		$this->objUser=new User;
	}

	public function showWelcome()
	{
		return View::make('hello');
	}
	

	public function register()
	{
		$data=Input::all();
		$validation=Validator::make($data,$this->rules);
		if($validation->fails()){
		
		echo json_encode(array('fail' =>$validation->messages()));
		exit;
		}
		if($data['confirmemail'] !=$data['email']){
			echo json_encode(array('fail' =>array("fail"=>"Email And Confirm Email Must Match")));
			exit;
		}
		if($data['password'] !=$data['confirmpassword']){
			echo json_encode(array('fail' =>array("fail"=>"Password And Confirm Password Must Match")));
			exit;
		}
		elseif($validation->passes()){

			$dataMatchArray=array(
				'email'  => $data['email']
				);

			   $this->objUser->email 	= $data['email'];
    		    $this->objUser->username = 'dummyuser';
    			$this->objUser->password = Hash::make($data['password']);
    			$this->objUser->save();
			$checkExistanceOfData=$this->objUser->checkExistance('tbl_registered_user_employer_detail',$dataMatchArray);
			if($checkExistanceOfData==True){
				echo json_encode(array("dataintigrity"=>array('dataintigrity'=>'Email Already Exists')));
			exit;
			}elseif($checkExistanceOfData==False)
			{
				$tablename="tbl_registered_user_employer_detail";
						$dataToInsert=$data;
			
						$registerUser=$this->objUser->insertData($tablename,$dataToInsert);
			
						if($registerUser==True){
			
							$message=json_encode(array("success"=>array("success"=>"User Registered")));
			
						}elseif ($registerUser==False) {
			
							$message=json_encode(array("fail"=>array("fail"=>"Fail To Register User")));
			
						}
						else{
			
							$message=json_encode(array("error"=>array("error"=>"We Found Some Thing Wrong,Contact Support")));
						}
			
						echo $message;
						exit;
					}
			else{
				echo json_encode(array("error"=>array('error'=>'We Found Some Thing Wrong,Contact Support')));
				exit;
			}
		}
		



	}


	public function createCategoryforJob()
	{
		$data=Input::all();
		$validation=Validator::make($data,$this->createCategory);
		if($validation->fails()){
			
		return json_encode(array("fail"=>$validation->messages()));
		}
		elseif($validation->passes()){
			$dataMatchArray=array(
				'title'  => $data['title']
				);
			$checkExistanceOfData=$this->objUser->checkExistance('tbl_category',$dataMatchArray);
			if($checkExistanceOfData==True){
				return json_encode(array("dataintigrity"=>array('dataintigrity'=>'Data Already Exists')));
			}
			elseif ($checkExistanceOfData==False) {
				
			$categoryCreate=$this->objUser->insertData('tbl_category',$data);
			if($categoryCreate==True){
				return json_encode(array("success"=>array('success'=>'Category Created')));
			}
			elseif ($categoryCreate==False) {
				return json_encode(array("fail"=>array('fail'=>'Fail To Create Category')));
			}else{
				return json_encode(array("error"=>array('error'=>'We Found Some Defficulties,Contact SUpport')));
			}
			}else{
				return json_encode(array("success"=>array('error'=>'We Found Some DIfficulties COnstact SUpport')));
			}



		}
	}
	public function postInternship()
	{
		$data=Input::all();
		$validation=Validator::make($data,$this->postJob);
		if($validation->fails()){
			return json_encode(array("fail"=>$validation->messages()));
		}
		elseif ($validation->passes()) {

			$postInternshipJob=$this->objUser->insertData('tbl_internship_posted',$data);

			if($postInternshipJob==True){
				return json_encode(array("success"=>array('success'=>'Job Posted')));

			}elseif ($postInternshipJob==False) {

				return json_encode(array("fail"=>array('Fail'=>'Fail To Post Job, Try After Some Time')));
				
			}else{
				return json_encode(array("error"=>array('error'=>'We Found Some Difficulties Constact Support')));
			}

			
		}
	}
	public function listjobposted()

	{
	
		$data=Input::all();
		if($data){
			$datatoSearch=array('id'=>$data['id']);
		}else{
			$datatoSearch=null;
		}
		$postInternshipJob=$this->objUser->getData('tbl_internship_posted',$datatoSearch)->get();


		if(sizeof($postInternshipJob)>0){
			return json_encode(array("success"=>$postInternshipJob));
		}
		elseif(sizeof($postInternshipJob)==0){
			return json_encode(array("fail" =>"No Record Found"));
		}
	}
	

	public function getregistereduser()
	{
		
		$data=Input::all();
		if(sizeof($data)>0){
			if(isset($data['clause'])){
			$arrayToSearch=json_decode($data['clause']);	
			}else{
				$arrayToSearch=null;
			}
			
		}else{
			//echo 'No data posted';
			$arrayToSearch=null;
			
		}
		
			$returndata=$this->objUser->getData('tbl_registered_user_employer_detail',$arrayToSearch)->get();	
		
			if(sizeof($returndata)>0){
				$response=array("success"=>$returndata);
			}elseif (sizeof($returndata)==0) {
				$response=array("fail"=>"No Record Found");
			}else{
				$response=array("error"=>"Error 500");
			}
		
		echo json_encode($response,JSON_PRETTY_PRINT);
	}

	public function getcategorylisted()
	{
		$categorylist=$this->objUser->getData('tbl_category',null)->get();
		if(sizeof($categorylist)>0){
			$response=array("success"=>$categorylist);
		}
		else if(sizeof($categorylist)==0){
			$response=array("fail"=>"No Records Found");
		}
		else{
			$response=array("error"=>"contact support");
		}
		return json_encode($response);
	}

	public function listactivecategory()
	{
		$categorylist=$this->objUser->getData('tbl_category',null)->select('display_title', 'id')->get();
		if(sizeof($categorylist)==0){
			return json_encode(array("fail"=>array("No Records Found")));
		}
		else if(sizeof($categorylist)>0)
		{
			return json_encode(array("success"=>$categorylist));
		}
	}
	
public function activedeactive()
{
	$datarecieved=Input::all();


/*print_r($datarecieved);
exit;*/
	if($datarecieved['toggleto']=='togglecategory'){

	$tablename="tbl_category";
	
	}
	elseif($datarecieved['toggleto']=='toggleregisteruser'){
		$tablename="tbl_registered_user_employer_detail";
	}
	elseif($datarecieved['toggleto']=='togglejobs') {
		$tablename="tbl_internship_posted";
	}
	
	if($datarecieved['status']=='active'){
		$changestatus  ='deactive';
		$updatemessage ="Category Deactivated";
	}
	else
	if($datarecieved['status']=='deactive'){
		$changestatus='active';
		$updatemessage ="Category Activated";
	}
	else{
		$changestatus='active';
		$updatemessage ="Category Activated";
	}

	$dataToUpdateArray=array(
		"status"     => $changestatus
		);
	$updateKey=array(
		'id' =>$datarecieved['id']
		);

	$activateDeactivate= $this->objUser->updateData($tablename,$dataToUpdateArray,$updateKey);
	if($activateDeactivate===True){
		return json_encode(array("success"=>$updatemessage));
	}
	elseif($activateDeactivate===False){
		return json_encode(array("Fail"=>$updatemessage));
	}

}



}
