<?php

class EmployerPaymentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $userObject;

	 function __construct(){


	 	$this->userObject=new User;
	 }



	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($userid)
	{
		//
		$pickData     =array('id','firstname','email','payment_plan_selected');
		$dataToSearch =array('id'=>$userid);

	    $showPaymentDone=$this->userObject->choseData('tbl_registered_user_employer_detail',$pickData,$dataToSearch)->get();

	    if(sizeof($showPaymentDone)>0){
	    	/*return json_encode(array('success'=>$showPaymentDone),JSON_PRETTY_PRINT);

*/
	    	foreach($showPaymentDone as $seePlan){
	    		if($seePlan->payment_plan_selected =='no'){
	    			return json_encode(array('selectedPlanDetail' =>'Please Select The Plan Of Your Choice' ),JSON_PRETTY_PRINT);
	    		}
	    		elseif($seePlan->payment_plan_selected=='yes'){
	    			//check whihc plan chosen

	    			$planChosen=$this->userObject->getData('employer_payment_plan',array('employer_id'=>$userid,'payment_success'=>'yes'))->limit(1)->orderBy('id', 'desc')->get();
	    			if(sizeof($planChosen)==0){
	    				$aboutPaymentPlan=array(

	    					'selectedPlanDetail'=>'[{no plan is associated with this user id,contact support}]'
	    					
	    					);
	    			}
	    			else if(sizeof($planChosen)>0){

  	    					$aboutPaymentPlan=array('selectedPlanDetail' =>$planChosen);

	    			}

	    		}
	    		else {
	    			$aboutPaymentPlan=array('error'=>'More Payment Option To DO.');
	    		}
	    	}


	    	$collectiveResponse=array_merge($showPaymentDone,$aboutPaymentPlan);
	    	return json_encode(array('success' => $collectiveResponse),JSON_PRETTY_PRINT);


	    }
	    else if(sizeof($showPaymentDone) ==0){
	    	return json_encode(array("fail" =>"We Can't Find A Matching Record"),JSON_PRETTY_PRINT);
	    }
	    else {
	    	return json_encode(array("error" => "Something Wen't Wrong.Contact Support."),JSON_PRETTY_PRINT);
	    }





	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    public function buyplan()
	 {
	 	$getPlanId=Input::all();

	 	if(sizeof($getPlanId)==0 && !isset($getPlanId['planId'])){

	 		return json_encode(array('error'=>'Some Data MissMatched'));
	 	}
	 	else if((sizeof($getPlanId)>0) && isset($getPlanId['planId'])){

	 		
	 		$userid =$getPlanId['userid'];
	 		$planId = $getPlanId['planId'];

	 		$checkExistancePlanId=$this->userObject->getData('payment_plan',array('id'=>$getPlanId['planId']))->get();
	 	
	 		if(sizeof($checkExistancePlanId)==0){
	 			return json_encode(array('fail' => 'This Plan Id Not Found In Record'),JSON_PRETTY_PRINT);
	 		}
	 		else if(sizeof($checkExistancePlanId) >0){
	 		
	 		 $chekUserAssociated=$this->userObject->getData('employer_payment_plan',array('employer_id'=>$userid,'plan_selected'=>$planId,'payment_success'=>'yes'))->limit(1)->orderBy('id', 'desc')->get();
              
              if(sizeof($chekUserAssociated)==0){
              
              	$dataToInsert=array(
              		'employer_id'   =>$userid,
              		'plan_selected'	=>$planId

              		);
              	
              	$alotPlan=$this->userObject->insertdata('employer_payment_plan',$dataToInsert);
              	if($alotPlan){
              		$message=array(
              			'message'  =>'First Attempt For This Plan',
              			'Response' =>'New Plan Aloted.');
              	}
              	
              	return json_encode(array('success'=>$message));
              }
              else if(sizeof($chekUserAssociated)>0){
              	return json_encode(array('success'=>'This User Already Selected This Perticular Plan,Want TO Renew?'));
             
                //renew plan




              }

	 		}
	 	}
	 	else {
	 		return json_encode(array('error' =>'Something went wrong,check you post data.'));
	 	}
	 }

	 public function knwCurrentStatysOfPaymentPlan()
	 {
	 	# code...

	 	/*
	 	1. check user exist or not 
		2. check user buied a plan or not
		3. show past plans and which is the active plans

		4. if buied show me which payment plan he buied

		5. how much job he has posted against this job id
		6. what is the status how much time left or how many post left
		*/
		$userid=Session::get('userid');
		echo Session::get('userid');
		echo $userid;
		if(!isset($userid)){
			return json_encode(array('fail' =>'userid missing'));
		}

		else if (isset($userid)){

		$userDetail=$this->userObject->getData('tbl_registered_user_employer_detail',array('id' => $userid))->get();

		if(sizeof($userDetail)==0){
			return json_encode(array('fail' =>'No Such User Found'.$userid),JSON_PRETTY_PRINT);
		}
		else if(sizeof($userDetail) > 0) {
		//	return json_encode(array('success'=>array("userFound" =>$userDetail)),JSON_PRETTY_PRINT);
			$userplanAssoc =$this->userObject->getData('employer_payment_plan',array('employer_id' => $userid,'payment_success'=>'yes'))->get();
			$currentPlan   = $this->userObject->getData('employer_payment_plan',array('employer_id' => $userid,'payment_success'=>'yes','currentPlan'=>'yes'))->orderBy('id', 'desc')->take(1)->get();
			if(sizeof($userplanAssoc)==0){
				return json_encode(array('fail'=>'User Didnot Purchased Any Plan'),JSON_PRETTY_PRINT);
			}
			else if(sizeof($userplanAssoc) > 0){
				return json_encode(array('success' => array(
					'userDetail'           =>$userDetail,
					'currentActivePlan'    =>$currentPlan,
					'userPurchasedHistory' =>$userplanAssoc

					)),JSON_PRETTY_PRINT);
			}



		}	
	
		}
		

	 }




}
