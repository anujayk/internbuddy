<?php

class userdashboard extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function login()
	{
		$rules=array(
			'email'=>"required",
			'password'=>"required"
			);
		$data=Input::all();
	
		$validator = Validator::make($data,$rules);
		
		if($validator->passes())
		{
		
			if(Auth::attempt($data)){
				Session::put('username',$data['email']);
				
				//echo Session::get('username');
				return json_encode(array('success'=>$data),JSON_PRETTY_PRINT);

			}
			else{
					$_SESSION['user'] =$data['email'];
			return json_encode(array('fail'=>'user pwd missmatch'));
		}
		//return Redirect::back()->withInput()->with('failure','username or password is invalid!');
		}
		
		else
		{
		return json_encode(array('fail'=>$validator->messages()));
		 
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
