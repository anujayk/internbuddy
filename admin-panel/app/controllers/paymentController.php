<?php

class paymentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $userObject;

	 function __construct(){

	 	$this->paymentRules=array(

	 		'plan_name'       =>'required',
	 		'plan_cost'       =>'required|numeric',
	 		'job_for_cost'    =>'required|numeric',
	 		'for_duration'    =>'required|in:month,year'
	 		);

	 	$this->userObject=new User;




	 }


	public function index()
	{
		$showPlan=$this->userObject->getData('payment_plan',array('status'=>'active'))->get();
		if(sizeof($showPlan)==0){
			return json_encode(array('fail'  => 'We Dont Have Any Payment Plan Yet'),JSON_PRETTY_PRINT);
		}
		else if(sizeof($showPlan)>0){
			return json_encode(array('success' =>$showPlan),JSON_PRETTY_PRINT);
		}
		else {
			return json_encode(array('error'  =>'Something went wrong contact support'));
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//

	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$data=Input::all();

		$validation=Validator::make($data,$this->paymentRules);
		if($validation->fails()){

			return json_encode(array('fail' =>$validation->messages()),JSON_PRETTY_PRINT);
		     exit;
		}
		else {


	        $checkPresence=$this->userObject->checkExistance('payment_plan',array('plan_name'=>$data['plan_name']));

	        if($checkPresence){
	        	return json_encode(array('fail' =>'Plan Name Already Exist,Chose Some Another'));
	        }

	       else if(!$checkPresence)
	       { 	
	       			$makePlan=$this->userObject->insertData('payment_plan',$data);
	       			if($makePlan){
	       				return json_encode(array('success' =>'New Plan Prepared'));
	       			}
	       }

		}



		 
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//


	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
