<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});
/*======================Login Related======================*/
Route::get('/',array('uses'=>'HomeController@login'));
Route::post('/login',array('uses'=>'userdashboard@Login'));
Route::get('/logout', array('uses' => 'HomeController@logout'));

/*==========================================================*/


Route::post('/validateEmailPhone','HomeController@validateEmailPhone');



/*===================Registered User Detail ==================*/

Route::post('/register','HomeController@register');
Route::get('/getregistereduser','HomeController@getregistereduser');

/* ===================================================
   					Internship Related
   ===================================================
   */
Route::post('/postInternship','HomeController@postInternship');
Route::get('/listjobposted','HomeController@listjobposted');

/*==============================================================================================
					CATEGORY RELATED
==============================================================================================*/

Route::post('/createCategoryforJob','HomeController@createCategoryforJob');
Route::get('/getcategorylisted','HomeController@getcategorylisted');
Route::get('/listactivecategory','HomeController@listactivecategory');



/*=======================================================
					Toggle Response
==========================================================
*/
Route::post('/activedeactive','HomeController@activedeactive');

/*

/*===============================================================
                          Payment Plan
   ==================================================*/


Route::resource('paymentplan','paymentController');



/*===============================================================
                          User Payment Related
   ==================================================*/



Route::resource('paymentdetail','EmployerPaymentController');
Route::resource('buyplan','EmployerPaymentController@buyplan');
Route::resource('knwCurrentStatysOfPaymentPlan','EmployerPaymentController@knwCurrentStatysOfPaymentPlan');



/*======================================================*/

/*
Route::get('login', array('uses' => 'HomeController@showLogin'));
	// route to process the form
Route::post('login', array('uses' => 'HomeController@doLogin'));
Route::post('logout', array('uses' => 'HomeController@doLogout'));

Route::any('/create',function(){
	$pwd="admin";
	$pwd=Hash::make($pwd);
	$arrayData=array(
		'email'       =>'admin@gmail.com',
		'username'    =>'admin',
		'password'    =>$pwd	
		);
	DB::table('users')->insert($arrayData);
});
*/