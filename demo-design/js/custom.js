
var app = angular.module('registerform', ['toaster']);

app.controller('registeruserdetail', function($scope, $http,toaster){
  
  $scope.userdata=function(e){
  	    e.preventDefault(); 
             
        $http({
            method:'POST',
            url:'/intern/admin-panel/public/register',
            data:$scope.registeruser,
           // headers: {'Content-Type': 'application/x-www-form-urlencoded'},


        }).success(function(response) {

       if(response.fail){
       	console.log(response)
       	angular.forEach(response.fail,function(key,value){
       		toaster.pop("error","",key);
       	});
       }
       else if(response.success){
       		angular.forEach(response.success,function(key,value){
       		toaster.pop("success","",key);
       	});
       }
       else if(response.error){
      
       	angular.forEach(response.error,function(key,value){
       		toaster.pop("Warning","",key);
       	});

       }else if(response.dataintigrity){
      		 console.log(response)
       			angular.forEach(response.dataintigrity,function(key,value){
       			toaster.pop("Warning","",key);
       	});	
       		}

       	else {
			console.log(response)
       		toaster.pop("Warning","","Contact Support");
       
       	}    
	    }).error(function(response){
    	console.log(response)
    });
    };
 
});

app.controller('categoryrelated',function($scope,$http,toaster){
	
	$http({
		method:"GET",
		url:'/intern/admin-panel/public/getcategorylisted'
	}).success(function(categorylist){
		toaster.pop("Warning","","Getting Category Lists");
		$scope.categorylist=categorylist;

	}).error(function(categorylist){
		toaster.pop("Warning","","Getting Category Lists");
		console.log(categorylist);
	});






$scope.addCategory=function(){

	console.log($scope.newcategory);

	$http({
		method:"post",
		url:"/intern/admin-panel/public/createCategoryforJob",
		data:$scope.newcategory
	}).success(function(returncatugrydata){
		 if(returncatugrydata.fail){
     
       	angular.forEach(returncatugrydata.fail,function(key,value){
       		toaster.pop("error","",key);
       	});
       }
       else if(returncatugrydata.success){
       		angular.forEach(returncatugrydata.success,function(key,value){
       		toaster.pop("success","",key);
       	});
       }
       else if(returncatugrydata.error){
      
       	angular.forEach(returncatugrydata.error,function(key,value){
       		toaster.pop("error","",key);
       	});

       }else if(returncatugrydata.dataintigrity){
      		 console.log(returncatugrydata)
       			angular.forEach(returncatugrydata.dataintigrity,function(key,value){
       			toaster.pop("Warning","",key);
       	});	
       		}

       	else {
			console.log(returncatugrydata)
       		toaster.pop("Warning","","Contact Support");
       
       	}  

	}).error(function(returncatugrydata){
		console.log(returncatugrydata);
	});

	}

  $scope.activedeactivecat=function(activitytodo,idtodo){

    $http({
      
      url    :'/intern/admin-panel/public/activedeactive',
      method :'POST',
      data   :{"id":idtodo,"status":activitytodo,"toggleto":"togglecategory"},
    }).success(function(toggleresponse){

      if(toggleresponse.success){
     
          toaster.pop("success","",toggleresponse);
       
   }
   else if(toggleresponse.fail){

        toaster.pop("error","",toggleresponse);
       
    
   }

    }).error(function(response){
    toaster.pop("error","","Some Error Occured Contact Support");

    });
   
  }


});

app.controller('freejobpost',function($scope,$http,toaster){


  $http({
    method  :"GET",
    url     :"/intern/admin-panel/public/listactivecategory"

  }).success(function(categorylist){

    $scope.activecategory=categorylist;

  }).error(function(categorylist){

    console.log("500: some error contact support");

  });

  $scope.postfreejob=function(f){
    f.preventDefault();
    console.log($scope.free);
    $http({
      url    :"/intern/admin-panel/public/postInternship",
      method :"POST",
      data   :$scope.free
    }).success(function(jobpostresponse){

        if(jobpostresponse.success){
          angular.forEach(jobpostresponse.success,function(freekey,freevalue){
          toaster.pop("success","",freekey);
        });
        }
        else if(jobpostresponse.fail){

          angular.forEach(jobpostresponse.fail,function(key,value){
          toaster.pop("error","",key);
        });
        }
        

    }).error(function(jobpostresponse){
        
          toaster.pop("error","",freekey);
        
    });
  }


  
});
app.controller('registereduser',function($scope,$http,toaster){

    $http({
      method    :"GET",
      url       :"/intern/admin-panel/public/getregistereduser"

    }).success(function(responseuserlist){
   
    if(responseuserlist.success){
      console.log('hi');
         $scope.userlist=responseuserlist;
    }else if(responseuserlist.fail){
         toaster.pop('error',responseuserlist.fail);
   
    }

    }).error(function(responseuserlist){
      console.log(responseuserlist);
    });


    $scope.viewdetailofuser= function(userid){
      console.log(userid);
      $http({
        method  :"GET",
        data    :userid,
        url     :"/intern/admin-panel/public/getregistereduser"
      }).success(function(useridresponse){
        $scope.userdetaillist=useridresponse;
        console.log(useridresponse);

      }).error(function(useridresponse){
        console.log('error logged here');
      });
    }

    $scope.activedeactivecat=function(activitytodo,idtodo){

    $http({
      
      url    :'/intern/admin-panel/public/activedeactive',
      method :'POST',
      data   :{"id":idtodo,"status":activitytodo,"toggleto":"toggleregisteruser"},
    }).success(function(toggleresponse){

      if(toggleresponse.success){
     
          toaster.pop("success","",toggleresponse);
       
   }
   else if(toggleresponse.fail){

        toaster.pop("error","",toggleresponse);
       
    
   }

    }).error(function(response){
    toaster.pop("error","","Some Error Occured Contact Support");

    });
   
  }

  });

app.controller('listpostedjob',function($scope,$http,toaster){
  $http({
    url  :"/intern/admin-panel/public/listjobposted",
    method :"GET"
  }).success(function(listjob){
    $scope.postedjobs=listjob;
  }).error(function(listjob){

  });

  

    $scope.viewdetailofuser= function(userid){
      console.log(userid);
      $http({
        method  :"GET",
        data    :userid,
        url     :"/intern/admin-panel/public/getregistereduser"
      }).success(function(useridresponse){
        $scope.userdetaillist=useridresponse;
        console.log(useridresponse);

      }).error(function(useridresponse){
        console.log('error logged here');
      });
    }


 $scope.viewmoredetail= function(jobuserid){
      //console.log(jobuserid);
      $http({
        method  :"GET",
        data    :jobuserid,
        url     :"/intern/admin-panel/public/listjobposted"
      }).success(function(useridresponsedetail){
        $scope.userinformation=useridresponsedetail;
        console.log($scope.userinformation);

      }).error(function(useridresponsedetail){
        console.log('error logged here');
      });
    }




   $scope.activedeactivecat=function(activitytodo,idtodo){

    $http({
      
      url    :'/intern/admin-panel/public/activedeactive',
      method :'POST',
      data   :{"id":idtodo,"status":activitytodo,"toggleto":"togglejobs"},
    }).success(function(toggleresponse){

      if(toggleresponse.success){
     
          toaster.pop("success","",toggleresponse);
       
   }
   else if(toggleresponse.fail){

        toaster.pop("error","",toggleresponse);
       
    
   }

    }).error(function(response){
    toaster.pop("error","","Some Error Occured Contact Support");

    });
   
  }
});